package is.hi.hbv501g2021supportsession;

import is.hi.hbv501g2021supportsession.Persistence.Entities.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
//@SpringBootTest
public class UserFitnessInfoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private User firstUser;

    private Workout workout;

    private Exercise e1;
    private Exercise e2;
    private Exercise e3;

    UserFitnessInfo userFitnessInfo;

    @Before
    public void setUp() {
        LoginInfo loginInfo = new LoginInfo();
        userFitnessInfo = new UserFitnessInfo();
        firstUser = new User("Þorsteinn", "thi35@hi.is", loginInfo, userFitnessInfo);

        e1 = new Exercise("Push ups", "", 5);
        e2 = new Exercise("Push ups", "", 10);
        e3 = new Exercise("Push ups", "", 20);
        List<Exercise> exerciseList = new ArrayList<Exercise>(){
            {add(e1); add(e2); add(e3);}
        };
        workout = new Workout("Medium swim", userFitnessInfo, Difficulty.MEDIUM, exerciseList, WorkoutType.SWIMMING);
    }

    @Test
    public void saveExerciseInUserFitnessInfo() {

        userFitnessInfo.addWorkout(workout);

        UserFitnessInfo savedUserfitnessInfo = this.entityManager.persistFlushFind(userFitnessInfo);

        assertThat(savedUserfitnessInfo.getId()).isNotNull();
        assertThat(savedUserfitnessInfo.getWorkouts().get(0).getWorkoutName()).isEqualTo("Medium swim");

    }

    @Test
    public void UnitTestGetWorkouts() {
        userFitnessInfo.addWorkout(workout);
        List<Workout> workoutList =  userFitnessInfo.getWorkouts();

        assertThat(workoutList.isEmpty()).isFalse();
        assertThat(workoutList.get(0).getWorkoutName()).isEqualTo("Medium swim");
    }


    @Test
    public void contextLoads() {
    }

}
