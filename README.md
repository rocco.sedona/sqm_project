# SQM_project
This project is a gym application that creates a workout for users from simple information. For example information about how often users work out and how intense the workouts are.

The project is written in java spring with a MVC setup and built with Maven.
The steps to be followed are the following:
* clone the repository with the command `git clone https://gitlab.com/rocco.sedona/sqm_project.git`
* navigate into the project folder
* if you want the snyk plugin to check for vulnerabilities, open the POM.xlm file and set the skip flat to true, otherwise leave it to false 
* create the JAR packages with the command `mvn package`. As Maven assumes ordering of phases, that executes also the intermediate compiling `mvn compile` and test `mvn test` phases (thus including unit testing with JUnit4 and code coverage with JaCoCo)
* to execute the application, run the command `java -jar HBV501G-2021-SupportSession-0.0.1-SNAPSHOT.jar`
* once the app is running, open a browser and check the webpage at: http://127.0.0.1:8080/
* you can delete the bytecode with the command `mvn clean` 

[![pipeline status](https://gitlab.com/rocco.sedona/sqm_project/badges/main/pipeline.svg)](https://gitlab.com/rocco.sedona/sqm_project/-/commits/main)
[![coverage report](https://gitlab.com/rocco.sedona/sqm_project/badges/main/coverage.svg)](https://gitlab.com/rocco.sedona/sqm_project/-/commits/main)

A [HTML website automatically generated](http://rocco.sedona.gitlab.io/sqm_project/dependency-management.html) by `mvn site` is available.

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=rocco.sedona_sqm_project&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=rocco.sedona_sqm_project)
